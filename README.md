# StyleTransfer-OUC-2021CVcourse

#### 介绍
本项目用于通过 K-Means 聚类等算法实现图像卡通化（风格迁移），  
也作为中国海洋大学2021秋季学期计算机视觉 Final Project.  
项目URL：https://gitee.com/hibiscus02/StyleTransfer-OUC-2021CVcourse  
讲解视频URL：https://www.bilibili.com/video/BV1uq4y127of?spm_id_from=333.999.0.0  

#### 项目规划  
1.  K-Means 聚类算法实现图像卡通化、黑白风格化（已实现）
2.  KNN 算法机器学习实现图像风格迁移（已实现）
3.  基于GAN的风格迁移，cycleGAN（未完成）

#### 结果实例
项目1-图像卡通化：  
![输入图片说明](Result/1_0001.jpg)  
![输入图片说明](Result/1_0002.jpg)  
![输入图片说明](Result/1_0003.jpg)  
项目1-图像黑白风格化：  
![输入图片说明](Result/2_0001.jpg)  
![输入图片说明](Result/2_0002.jpg)
项目2-指定图像文件的风格迁移：
![输入图片说明](Result/TheStarryNight.jpg) + ![输入图片说明](Result/0003.jpg)  
![输入图片说明](Result/after.png)  



#### 软件架构
PaddlePaddle  
Python == 3.7  
Numpy == 1.16.4  
Opencv == 4.1.1.26  
Scikit-image == 0.19.1  
Sklearn

#### 使用说明

0.  将 cell 中代码放入 PaddlePaddle 的代码块中
1.  项目1需要在 PaddlePaddle 平台在有 numpy、opencv 的环境中执行
2.  项目2需要在 PaddlePaddle 平台在有 sklearn、scikit-image、numpy 的环境中执行
3.  ···


#### 参与贡献

0.  小组成员：路芸蔓（组长）、张淑君、赵文涛
1.  Fork 本仓库
2.  提交代码
3.  新建 Pull Request

